variable "qemu_server" {
  type    = string
  default = "ha-datacenter"
}

variable "qemu_user" {
  type    = string
  default = "root"
}

variable "qemu_password" {
  type    = string
  default = "password"
}

variable "datacenter" {
  type    = string
  default = "Virtual Machines"
}

variable "cluster" {
  type    = string
  default = ""
}

variable "datastore" {
  type    = string
  default = "virtio"
}

variable "network_name" {
  type    = string
  default = "virtio-net"
}

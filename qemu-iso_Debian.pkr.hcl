source "qemu" "this" {

  accelerator = "kvm"
  headless = false
  shutdown_command = "echo 'brtec' | sudo -S /usr/sbin/shutdown -P now"
  qemu_binary = "/usr/bin/qemu-system-x86_64"
  vm_name = "debian-11-base"
  //guest_os_type = "Debian_64"
  //output_directory = "debian11-base-img"
  vnc_bind_address = "0.0.0.0"

  http_directory = "http"
  http_port_min = 10082
  http_port_max = 10089
  host_port_min = 2222
  host_port_max = 2229

  ssh_host = "192.168.8.4"
  ssh_username = "drolen"
  ssh_password = "111"
  ssh_port = "22"
  ssh_wait_timeout = "1200s"

  
  disk_interface = "virtio"
  disk_size = 30000
  disk_cache = "none"
  disk_compression = true
  disk_discard = "unmap"
  format = "qcow2"
  net_device = "virtio-net"

  iso_url = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso"
  iso_checksum = "2810f894afab9ac2631ddd097599761c1481b85e629d6a3197fe1488713af048d37241eb85def681ba86e62b406dd9b891ee1ae7915416335b6bb000d57c1e53"

  boot_wait = "2s"

  qemuargs = [
    [ "-m", "2048M" ],
    [ "-smp", "cpus=1,maxcpus=2,cores=2" ],
    [ "-boot", "strict=on" ],
    [ "-netdev", "user,id=mynet0" ],
    [ "-device", "virtio-net,netdev=mynet0" ]
  ]


  boot_command = [
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
        " auto-install/enable=true ",
        " locale=en_US.UTF-8 <wait>",
        " priority=critical",
        "<enter><wait>"
  ]
}

build {
  sources  = [
    "source.qemu.this"
  ]

  provisioner "shell-local" {
    inline  = ["echo the address is: $PACKER_HTTP_ADDR and build name is: $PACKER_BUILD_NAME"]
  }
}
